from flask.cli import FlaskGroup

from main import app, db, User

cli = FlaskGroup(app)


@cli.command("create_db")
def create_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command("seed_db")
def seed_db():
    db.session.add(User(email="michael@mherman.org", first_name="Michael", last_name="Herman"))
    db.session.commit()


if __name__ == "__main__":
    cli()
