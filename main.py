import os

from flask import Flask, render_template
import sqlite3
from faker import Faker
from flask_sqlalchemy import SQLAlchemy

fake = Faker()
app = Flask(__name__)
app.config.from_object("config.Config")

app_label = os.getenv("APP_LABEL", "UNKNOWN")

db = SQLAlchemy(app)


class User(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(128), unique=True, nullable=False)
    first_name = db.Column(db.String(128), unique=False, nullable=True)
    last_name = db.Column(db.String(128), unique=False, nullable=True)
    active = db.Column(db.Boolean(), default=True, nullable=False)

    def __init__(self, email, first_name, last_name):
        self.email = email
        self.first_name = first_name
        self.last_name = last_name


@app.route("/")
def hello_world():
    return render_template('index.html')


@app.route("/info")
def info():
    return f"Hello from {app_label}"


@app.route("/users")
def users():
    all_users = User.query.all()
    res = [(u.email, u.first_name, u.last_name) for u in all_users]
    return render_template("users.html", users=res)


@app.route("/users/add")
def add_user():
    email = fake.email()
    fname = fake.first_name()
    lname = fake.last_name()
    db.session.add(User(email=email, first_name=fname, last_name=lname))
    db.session.commit()
    return render_template("add_user.html", email=email, fname=fname, lname=lname)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000)
